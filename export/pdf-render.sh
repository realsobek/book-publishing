#!/bin/bash

#: Title       : Pepper&Carrot KRArender
#: Author      : David REVOY < info@davidrevoy.com >, Mjtalkiewicz (aka Player_2)
#: License     : GPL

# TODO: Fix later a cryptic warning from Ghostscript; I'll not take the risk of changing my line for the moment because it works:
# **** WARNING: The .setpdfwrite operator has been deprecated and will be removed entirely
#               in the next release of Ghostscript. The functionality of this operator has
#               been reduced to increasing the size of the VM threshold. If you believe you
#               have a real need for this then you should replace your call to .setpdfwrite
#               with:
# 
#               3000000 setvmthreshold

# TODO: Fix parallel call, something new...
# parallel: Warning: Input is read from the terminal. You either know what you
# parallel: Warning: are doing (in which case: YOU ARE AWESOME!) or you forgot
# parallel: Warning: ::: or :::: or to pipe data into parallel. If so
# parallel: Warning: consider going through the tutorial: man parallel_tutorial
# parallel: Warning: Press CTRL-D to exit.



scriptversion="0.1"

# Custom folders names:
export folder_cache="cache"
export folder_export="exports"

# Utils
export projectname="${PWD##*/}"
export workingpath="${PWD}"
export isodate=$(date +%Y-%m-%d)
export version=$(date +%Y-%m-%d_%Hh%M)
export versiondaily=$(date +%Y%m%d)
export Off=$'\e[0m'
export Purple=$'\e[1;35m'
export Blue=$'\e[1;34m'
export Green=$'\e[1;32m'
export Red=$'\e[1;31m'
export Yellow=$'\e[1;33m'
export White=$'\e[1;37m'
export BlueBG=$'\e[1;44m'
export RedBG=$'\e[1;41m'
export PurpleBG=$'\e[1;45m'
export Black=$'\e[1;30m'

# Windows title
printf "\033]0;%s\007\n" "*Render: $projectname"

clear

_display_ui()
{
  echo ""
  echo " ${White}${BlueBG}                                                                ${Off}"
  echo " ${White}${BlueBG}                       -= PDF Renferfarm =-                     ${Off}"
  echo " ${White}${BlueBG}                                                                ${Off}"
  echo ""
  echo " * version: $scriptversion "
  echo " * projectname: $projectname "
  echo " * workingpath: ${workingpath} "
  echo ""
}

_dir_creation()
{
  cd ${workingpath}
  
  if [ -d "${workingpath}/${folder_cache}" ]; then
    echo " * ${folder_cache} found" 
  else
    echo "${Green} * creating folder: ${folder_cache} ${Off}"
    mkdir -p ${workingpath}/"${folder_cache}"
  fi

  if [ -d "${workingpath}/${folder_export}" ]; then
    echo " * ${folder_export} found" 
  else
    echo "${Green} * creating folder: ${folder_export} ${Off}"
    mkdir -p ${workingpath}/${folder_export}
  fi

  if [ -d "${workingpath}/${folder_exportcmyk}" ]; then
    echo " * ${folder_exportcmyk} found" 
  else
    echo "${Green} * creating folder: ${folder_exportcmyk} ${Off}"
    mkdir -p ${workingpath}/${folder_exportcmyk}
  fi
}

_render_pdf_work()
{
  PdfFile=$1
  cd ${workingpath}
  txtfile=$(echo ${PdfFile}|sed 's/\(.*\)\..\+/\1/')"pdfrender.txt"
  PdfCover=$(echo ${PdfFile}|sed 's/\(.*\)\..\+/\1/')"_cover.pdf"
  PdfBackCover=$(echo ${PdfFile}|sed 's/\(.*\)\..\+/\1/')"_backcover.pdf"
  PdfTmpFolder=$(echo ${PdfFile}|sed 's/\(.*\)\..\+/\1/')""
  # Read the checksum of *.pdf file
  md5read="`md5sum ${PdfFile}`"
  
  # Avoid grep to fail if no file found
  if [ -f ${workingpath}/"${folder_cache}"/"${txtfile}" ]; then
    true
  else
    touch ${workingpath}/"${folder_cache}"/"${txtfile}"
  fi
  
  # Compare if actual *.pdf checksum is similar to the previous one recorded on txtfile
  if grep -q "$md5read" ${workingpath}/"${folder_cache}"/"${txtfile}"; then
    echo " ==> [pdf] ${PdfFile} file is up-to-date."
  else
    echo "${Blue} * [cache info] ${PdfFile} is new or modified, rendered. ${Off}"

    # Update the cache with a new version
    md5sum ${PdfFile} > ${workingpath}/"${folder_cache}"/"${txtfile}"

    if [[ ${PdfFile} == *"inside"* ]]; then
      echo "${Blue} * Rendering CMYK sanified version for inside: ${PdfFile} . ${Off}"
      PdfCMYKfinal=$(echo ${PdfFile}|sed 's/\(.*\)\..\+/\1/')"_CMYK-sanified.pdf"

      gs -sDEVICE=pdfwrite -dPDFX -dCompatibilityLevel=1.3 -sOutputFile="${workingpath}/${folder_export}/${PdfCMYKfinal}" -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -dPDFSETTINGS=/prepress -c ".setpdfwrite << /ColorACSImageDict << /VSamples [ 1 1 1 1 ] /HSamples [ 1 1 1 1 ] /QFactor 0.13 /Blend 1 >> /ColorImageDownsampleType /Bicubic /ColorConversionStrategy /LeaveColorUnchanged >> setdistillerparams" -f "${workingpath}/${PdfFile}"
    fi

    if [[ ${PdfFile} == *"hardcover"* ]]; then
      BookId="${PdfFile:0:6}"
      PdfCovertmp=$(echo ${BookId}|sed 's/\(.*\)\..\+/\1/')"_covertmp.pdf"
      PdfCover=$(echo ${BookId}|sed 's/\(.*\)\..\+/\1/')"_cover.pdf"
      PdfBackCovertmp=$(echo ${BookId}|sed 's/\(.*\)\..\+/\1/')"_backcovertmp.pdf"
      PdfBackCover=$(echo ${BookId}|sed 's/\(.*\)\..\+/\1/')"_backcover.pdf"
      JpgCover=$(echo ${BookId}|sed 's/\(.*\)\..\+/\1/')"_cover.jpg"

      # Rendering cover
      # Good tool but mesured in... point
      # Also the coordinate are not like CSS margin/padding/border; it is disturbing:
      # the four values describes : left, top, right, bottom.
      echo "${Green} ==> [hardcover] ${BookId}: extracting a cover hi-res PDF in cache. ${Off}"
      echo "* Cropping a PDF cover."
      pdfcrop --margins '-960 -55 -40 -100' "${workingpath}/${PdfFile}" "${workingpath}/${folder_cache}/${PdfCovertmp}"

      # export a jpg picture of the cover for the shop
      echo "* Exporting cover as JPG."
      convert -density 300 "${workingpath}/${folder_cache}/${PdfCovertmp}" -resize 990x -profile ${workingpath}/../src/icc/sRGB-elle-V2-srgbtrc.icc "${workingpath}/${folder_export}/${JpgCover}"

      # Optimisation: sRGB ebook version in cache
      echo "* Optimizing PDF cover as ebook."
      gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.3 -sOutputFile="${workingpath}/${folder_cache}/${PdfCover}" -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -q -dPDFSETTINGS=/ebook -dColorImageResolution=200 -f "${workingpath}/${folder_cache}/${PdfCovertmp}"

      #   Generate a backcover PDF (in cache)
      echo "* Cropping a PDF backcover."
      pdfcrop --margins '-280 -55 -740 -100' "${workingpath}/${PdfFile}" "${workingpath}/${folder_cache}/${PdfBackCovertmp}"

      # Optimisation: sRGB ebook version in cache
      echo "* Optimizing PDF backcover as ebook."
      gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.3 -sOutputFile="${workingpath}/${folder_cache}/${PdfBackCover}" -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -q -dPDFSETTINGS=/ebook -dColorImageResolution=200 -f "${workingpath}/${folder_cache}/${PdfBackCovertmp}"

      # Clean-up tmp:
      if [ -f "${workingpath}/${folder_cache}/${PdfCovertmp}" ]; then
        rm "${workingpath}/${folder_cache}/${PdfCovertmp}"
      fi 
      if [ -f "${workingpath}/${folder_cache}/${PdfBackCovertmp}" ]; then
        rm "${workingpath}/${folder_cache}/${PdfBackCovertmp}"
      fi 
    else
      # Rendering sRGB ebook version in cache
      echo "${Green} ==> ${PdfFile}: rendering a ebook version in cache. ${Off}"
      gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.3 -sOutputFile="${workingpath}/${folder_cache}/${PdfFile}" -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -q -dPDFSETTINGS=/ebook -dColorImageResolution=200 -f "${workingpath}/${PdfFile}"
    fi
  fi
}

_render_pdf()
{
  # Method to update change in graphical file
  # Trying to be smart and consume the less power, but more disk space.
  # Only file changed are reprocessed thanks to the folder cache

  echo "${Yellow} [RENDER PDF]${Off}"
  echo "${Yellow} =-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= ${Off}"
  
  # Project should contain *.pdf artworks anyway
  cd ${workingpath}
  export -f _render_pdf_work
  ls -1 *.pdf | parallel _render_pdf_work "{}"
}

_concatenate_pdf_work()
{
  PdfFile=$1
  cd ${workingpath}/${folder_cache}
  txtfile=$(echo ${PdfFile}|sed 's/\(.*\)\..\+/\1/')"pdfconcatenate.txt"
  PdfOutput=$(echo ${PdfFile}|sed 's/\(.*\)\..\+/\1/')"_final.pdf"
  PdfTmpFolder=$(echo ${PdfFile}|sed 's/\(.*\)\..\+/\1/')""
  PdfFileSampleoutput=$(echo ${PdfFile}|sed 's/\(.*\)\..\+/\1/')"_sample.pdf"

  # Read the checksum of *.pdf file
  md5read="`md5sum ${PdfFile}`"
  
  # Avoid grep to fail if no file found
  if [ -f ${workingpath}/"${folder_cache}"/"${txtfile}" ]; then
    true
  else
    touch ${workingpath}/"${folder_cache}"/"${txtfile}"
  fi
  
  # Compare if actual *.pdf checksum is similar to the previous one recorded on txtfile
  if grep -q "$md5read" ${workingpath}/"${folder_cache}"/"${txtfile}"; then
    echo " ==> [pdf] ${PdfFile} file is up-to-date."
  else

    # Update the cache with a new version
    md5sum ${PdfFile} > "${workingpath}/${folder_cache}/${txtfile}"

    if [[ ${PdfFile} == *"inside"* ]]; then
      echo "${Blue} * [cache info] ${PdfFile} is new or modified, concatenation. ${Off}"
      BookId="${PdfFile:0:6}"
      PdfCover=$(echo ${BookId}|sed 's/\(.*\)\..\+/\1/')"_cover.pdf"
      PdfBackCover=$(echo ${BookId}|sed 's/\(.*\)\..\+/\1/')"_backcover.pdf"

      # concatenate
      # pdftk blank.pdf src.pdf cat output res.pdf
      echo "==> glueing. ${Off}"
      pdftk "${workingpath}/${folder_cache}/${PdfCover}" "${workingpath}/${folder_cache}/${PdfFile}" "${workingpath}/${folder_cache}/${PdfBackCover}" cat output "${workingpath}/${folder_export}/${PdfOutput}"

      # sample preview
      echo "==> creating sample (first 10 pages). ${Off}"
      pdftk "${workingpath}/${folder_export}/${PdfOutput}" cat 1-10 71-end output "${workingpath}/${folder_export}/${PdfFileSampleoutput}"
    fi
  fi
}

_concatenate_pdf()
{
  # Method to update change in graphical file
  # Trying to be smart and consume the less power, but more disk space.
  # Only file changed are reprocessed thanks to the folder cache

  echo "${Yellow} [CONCATENATE PDF]${Off}"
  echo "${Yellow} =-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= ${Off}"
  
  # Project should contain *.pdf artworks anyway
  cd "${workingpath}/${folder_cache}"
  export -f _concatenate_pdf_work
  ls -1 *.pdf | parallel _concatenate_pdf_work "{}"
}

# Runtime counter: start
renderfarm_runtime_start=$(date +"%s")

# Execute
_display_ui
_dir_creation
_render_pdf
_concatenate_pdf

# Runtime counter: end and math
renderfarm_runtime_end=$(date +"%s")
diff_runtime=$(($renderfarm_runtime_end-$renderfarm_runtime_start))

# End User Interface messages
echo ""
echo " * $projectname rendered in $(($diff_runtime / 60))min $(($diff_runtime % 60))sec."
echo ""

# Notification for system when out-of-focus
notify-send "PDFRender" "$projectname rendered in $(($diff_runtime / 60))min $(($diff_runtime % 60))sec."

# Windows title
printf "\033]0;%s\007\n" "Render: $projectname"

# Task is executed inside a terminal
# This line prevent terminal windows to be closed
# Necessary to read log later
# Print a end
echo "${Blue}================== JOB DONE ==================${Off}"
echo "${Purple}  Press [Enter] to launch the script again or:"
echo "${White}${BlueBG}[q]${Off} => to Quit. ${Off}"
read -p "?" ANSWER
if [ "$ANSWER" = "q" ]; then
  exit
else
  # Launch again
  cd ${workingpath}
  ${workingpath}/pdf-render.sh
fi


