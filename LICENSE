LICENSE
=======

Thank you for your interest into the license of the book-publishing project of 
Pepper&Carrot. You can reuse the files here according to the license:
**Creative Commons Attribution 4.0 International (CC BY 4.0)**.
You'll find a human-readable summary here under. 

/!\IMPORTANT NOTES/!\: don't forget any attribution to contributors!  
→ Proofreaders, script doctors, translators, scenarists but also the 
contributors of the Hereva universe. I'm asking only that, so **respect it**.
 Follows the best practise I advice here:   
[https://www.davidrevoy.com/article605/best-practices-for-attribution]

# Rules:

You are free to:
----------------

* Share — copy and redistribute the material in any medium or format.  
* Adapt — remix, transform, and build upon the material for any purpose, even
 commercially. 

The licensor cannot revoke these freedoms as long as you follow the license 
terms.

Under the following terms:
--------------------------

* Attribution — You must:  
  → give appropriate credit  
  → provide a link to the license  
  → ... and indicate if changes were made  

  You may do so in any reasonable manner, but not in any way that suggests 
  the licensor endorses you or your use.

* No additional restrictions  
  → You may not apply legal terms or technological measures that legally
  restrict others from doing anything the license permits.
  
Full License:
-------------

The full detailed version is here: 
[https://creativecommons.org/licenses/by/4.0/legalcode]

