
## Page 1

Pepper&Carrot
Bigtitle: Book 2: The Witches of Chaosah


## Page 2

Copyright © David Revoy 2020.
www.peppercarrot.com
This work is licensed under a Creative Commons Attribution 4.0 International license.
The text of the license is available at https://creativecommons.org/licenses/by/4.0/.

Sources:
Artworks, translations and more can be found here: https://www.peppercarrot.com/static6/sources.

Attribution:

Hereva, the universe of Pepper&Carrot:
Creation: David Revoy.
Lead maintainer: Craig Maloney.
Writers: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin.
Correctors: Alex Gryson, CGand, Hali, Moini, Willem Sonke.

Project management:
Technical maintenance and scripting: CalimeroTeknik, Craig Maloney , David Revoy, Gleki Arxokuna, GunChleoc, Jookia, Mjtalkiewicz (aka Player_2), Nicolas Artance, Noel, Quetzal2, Valvin, Xhire.
General maintenance of the database of SVGs: 3dnett, CalimeroTeknik, Châu, David Revoy, GunChleoc, Karl Ove Hufthammer, Marc "Mc" Jeanmougin, Midgard.
Website maintenance and new features: David Revoy, GunChleoc, Jookia, Valvin (on the top of a modified PluXML with plugin plxMyMultiLingue).

Book-publishing project specific:
Proofreading: Ángel, Arlo James Barnes, Craig Maloney, John Morahan, Martin Disch, Paul, Parnikkapore, Stephen Paul Weber, RW, Valvin.
Approved for free culture seal logo on the cover by Romaine.

Episode 12: Autumn Clearout (published on 2015-10-31)
Art: David Revoy.   Scenario: David Revoy.
Translation [English]: Alex Gryson.
Proofreading: GunChleoc.

Episode 13: The Pyjama Party (published on 2015-12-08)
Art: David Revoy.   Scenario: David Revoy.
Translation [English]: Alex Gryson.

Episode 14: The Dragon's Tooth (published on 2016-01-29)
Art: David Revoy.   Scenario: David Revoy.
Translation [English]: Alex Gryson.
Proofreading: Rémi Rampin.

Episode 15: The Crystal Ball (published on 2016-03-25)
Art: David Revoy.   Scenario: David Revoy.
Translation [English]: David Revoy.
Proofreading: Rémi Rampin.

Episode 16: The Sage of the Mountain (published on 2016-04-30)
Art: David Revoy.   Scenario: David Revoy.
Translation [English]: Alex Gryson.
Proofreading: Irina.

Episode 17: A Fresh Start (published on 2016-06-30)
Art: David Revoy.   Scenario: David Revoy.
Translation [English]: Alex Gryson.

Episode 18: The Encounter (published on 2016-08-05)
Art: David Revoy.   Scenario: David Revoy.
Inspiration: Two scenarios by Craig Maloney "You found me, I choose You" and "Visit from Hippiah".
Translation [English]: Alex Gryson.

Episode 19: Pollution (published on 2016-10-26)
Art: David Revoy.   Scenario: David Revoy.
Script-doctor: Craig Maloney.
Inspiration: Scenario of Juan José Segura "the book of secrets".
Beta-readers: Alex Gryson, Nicolas Artance, Seblediacre, Valvin.
Translation [English]: Alex Gryson.
Proofreading: GunChleoc.

Episode 20: The Picnic (published on 2016-12-17)
Art: David Revoy.   Scenario: David Revoy.
Translation [English]: David Revoy.
Proofreading: Alex Gryson.

Episode 21: The Magic Contest (published on 2017-02-23)
Art: David Revoy.   Scenario: David Revoy.
Beta-readers: Craig Maloney, Jookia, Nicolas Artance, R J Quiralta, talime, Valvin.
Translation [English]: Alex Gryson.
Proofreading: Craig Maloney, sn0wmonster.

Special thanks to:

All patrons of Pepper&Carrot! Your support made all of this possible.

All translators of Pepper&Carrot for their contributions:
Aldrian Obaja Muis, Alexandre Alapetite, Alexandre E. Almeida, Alex Filonenko, Alex Gryson, Alina The Hedgehog, Andrej Ficko, Andrew "Akari" Alexeyew, Antonio Parisi, Arild Torvund Olsen, Aslan Zamanloo, Binh Pham, Bonaventura Aditya Perdana, CalimeroTeknik, Carlo Gandolfi, colognella, Costin-Sorin Ionescu, Craig Maloney, David Revoy, Denis Prisukhin, Dika Michio, DikaMikaelMichio, Dimitris Axiotis, Emmiline Alapetite, Erwhann-Rouge / webmistusik, Erwhann-Rouge, Florin Sandru, František Veverka, Frederico Batista, George Karettas, Gilles-Philippe Morin, Giovanni Alfredo Garciliano Díaz, Gleki Arxokuna, grin, Grzegorz Kulik, Guillaume Lestringant, GunChleoc, guruguru, Gwenaëlle Lefeuvre, Gwenaëlle Lefeuvre's Dad, Halász Gábor "Hali", Helmar Suschka, Hongpyo Jang, Hulo Biral the Tomcat, Hồ Nhựt Châu, Ionicero, Jihoon Kim, Jorge Maldonado Ventura, Juanjo Faico, Juan José Segura, Julian Eric Ain, Julien R. Mauduit, Kaoseto, Kari Lehto, Karl Ove Hufthammer, karxusa, Kateřina Fleknová, Kim Jihoon, Konstantin Morenko, KuroDash, Leonid "Deburger" Androschuk, Lou Simons, Mahwiii, Marie Moestrup, Marina, Marko Ivancevic, Martin Disch, Martin Doucha, Midgard, Mikael Olofsson, Milena Petrovic, Mishvanda, Muhammad Nur Hidayat MNH48, Name, NayLedofich, Neha Deogade, Nicolas Artance, Nikitin Stanislav, Paolo Abes, Patrik Deriano, Peter Moonen, Philipp (hapit) Hemmer, Pranav Jerry, Quetzal2, Rafael Carreras, Ran Zhuang, Rebecca Breu, Reinaldo Calvin, Ret Samys, R J Quiralta, Scott Walters, Shikamaru "initbar" Yamamoto, sitelen, StarWish, StarWish, Stefan Carpentier, Steve Harris, Sölve Svartskogen, talime, Tharinda Divakara, Thomas Nordstrøm, Tirifto, Valvin, Willem Sonke, William Johnsson, Yan Jing, Zdenek Orsag.

The Framasoft team for hosting our oversized repositories via their Gitlab instance Framagit.

The Freenode staff for our #pepper&carrot IRC community channel.

All Free/Libre and open-source software because all Pepper&Carrot episodes are created using 100% Free/Libre software on a GNU/Linux operating system. The main ones used for production are:
- Krita for artworks (krita.org).
- Inkscape for vector and speechbubbles (inkscape.org).
- Blender for arwork and video editing (blender.org).
- Kdenlive for video editing (kdenlive.org).
- Scribus for the book project (scribus.net).
- Gmic for filters and effects (gmic.eu).
- Imagemagick & Bash for 90% of automation for the project.

And finally to all developers who interacted on fixing Pepper&Carrot specific bug-reports:
A.l.e (Scribus), Boudewijn Rempt (Krita) , David Tschumperlé (Gmic), Dmitry Kazakov (Krita) , Jean Ghali (Scribus), Marc Jeanmougin (Inkscape), Tavmjong Bah (Inkscape), Wolthera van Hövell tot Westerflier (Krita).

... and anyone I've missed.


## Page 68

To be continued...


## Page 71

Version 1
Desktop publishing done with Scribus 1.4.8 on Kubuntu Linux 19.10
Montauban, France - July, 2020
www.davidrevoy.com

www.peppercarrot.com
