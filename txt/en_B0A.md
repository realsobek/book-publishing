
## Page 1

Bigtitle: The Art of Pepper&Carrot


## Page 2

Copyright © David Revoy 2020.
www.peppercarrot.com
This work is licensed under a Creative Commons Attribution 4.0 International license.
The text of the license is available at https://creativecommons.org/licenses/by/4.0/.

Sources:
Artworks, translations and more can be found here: https://www.peppercarrot.com/static6/sources.

Attribution:

Artworks, desktop publishing and text:
David Revoy

Hereva, the universe of Pepper&Carrot:
Creation: David Revoy.
Lead maintainer: Craig Maloney.
Writers: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin.
Correctors: Alex Gryson, CGand, Hali, Moini, Willem Sonke.

Project management:
Technical maintenance and scripting: CalimeroTeknik, Craig Maloney , David Revoy, Gleki Arxokuna, GunChleoc, Jookia, Mjtalkiewicz (aka Player_2), Nicolas Artance, Noel, Quetzal2, Valvin, Xhire.

General maintenance of the database of SVGs: 3dnett, CalimeroTeknik, Châu, David Revoy, GunChleoc, Karl Ove Hufthammer, Marc "Mc" Jeanmougin, Midgard.

Website maintenance and new features: David Revoy, GunChleoc, Jookia, Valvin (on the top of a modified PluXML with plugin plxMyMultiLingue).

Book-publishing project specific:
Proofreading: Ángel, Arlo James Barnes, Craig Maloney, John Morahan, Martin Disch, Paul, Parnikkapore, Stephen Paul Weber, RW, Valvin.
Approved for free culture seal logo on the cover by Romaine.

Special thanks to:

All patrons of Pepper&Carrot! Your support made all of this possible.

All translators of Pepper&Carrot for their contributions:
Aldrian Obaja Muis, Alexandre Alapetite, Alexandre E. Almeida, Alex Filonenko, Alex Gryson, Alina The Hedgehog, Andrej Ficko, Andrew "Akari" Alexeyew, Antonio Parisi, Arild Torvund Olsen, Aslan Zamanloo, Binh Pham, Bonaventura Aditya Perdana, CalimeroTeknik, Carlo Gandolfi, colognella, Costin-Sorin Ionescu, Craig Maloney, David Revoy, Denis Prisukhin, Dika Michio, DikaMikaelMichio, Dimitris Axiotis, Emmiline Alapetite, Erwhann-Rouge / webmistusik, Erwhann-Rouge, Florin Sandru, František Veverka, Frederico Batista, George Karettas, Gilles-Philippe Morin, Giovanni Alfredo Garciliano Díaz, Gleki Arxokuna, grin, Grzegorz Kulik, Guillaume Lestringant, GunChleoc, guruguru, Gwenaëlle Lefeuvre, Gwenaëlle Lefeuvre's Dad, Halász Gábor "Hali", Helmar Suschka, Hongpyo Jang, Hulo Biral the Tomcat, Hồ Nhựt Châu, Ionicero, Jihoon Kim, Jorge Maldonado Ventura, Juanjo Faico, Juan José Segura, Julian Eric Ain, Julien R. Mauduit, Kaoseto, Kari Lehto, Karl Ove Hufthammer, karxusa, Kateřina Fleknová, Kim Jihoon, Konstantin Morenko, KuroDash, Leonid "Deburger" Androschuk, Lou Simons, Mahwiii, Marie Moestrup, Marina, Marko Ivancevic, Martin Disch, Martin Doucha, Midgard, Mikael Olofsson, Milena Petrovic, Mishvanda, Muhammad Nur Hidayat MNH48, Name, NayLedofich, Neha Deogade, Nicolas Artance, Nikitin Stanislav, Paolo Abes, Patrik Deriano, Peter Moonen, Philipp (hapit) Hemmer, Pranav Jerry, Quetzal2, Rafael Carreras, Ran Zhuang, Rebecca Breu, Reinaldo Calvin, Ret Samys, R J Quiralta, Scott Walters, Shikamaru "initbar" Yamamoto, sitelen, StarWish, StarWish, Stefan Carpentier, Steve Harris, Sölve Svartskogen, talime, Tharinda Divakara, Thomas Nordstrøm, Tirifto, Valvin, Willem Sonke, William Johnsson, Yan Jing, Zdenek Orsag.

The Framasoft team for hosting our oversized repositories via their Gitlab instance Framagit.

The Freenode staff for our #pepper&carrot IRC community channel.

All Free/Libre and open-source software because all Pepper&Carrot episodes are created using 100% Free/Libre software on a GNU/Linux operating system. The main ones used for production are:
- Krita for artworks (krita.org).
- Inkscape for vector and speechbubbles (inkscape.org).
- Blender for arwork and video editing (blender.org).
- Kdenlive for video editing (kdenlive.org).
- Scribus for the book project (scribus.net).
- Gmic for filters and effects (gmic.eu).
- Imagemagick & Bash for 90% of automation for the project.

And finally to all developers who interacted on fixing Pepper&Carrot specific bug-reports:
A.l.e (Scribus), Boudewijn Rempt (Krita) , David Tschumperlé (Gmic), Dmitry Kazakov (Krita) , Jean Ghali (Scribus), Marc Jeanmougin (Inkscape), Tavmjong Bah (Inkscape), Wolthera van Hövell tot Westerflier (Krita).

... and anyone I've missed.


## Page 3

Table of Contents

Pepper&Carrot, the beginnings...	5
The Magic Schools...............15
     Chaosah....................16
     Magmah.....................20
     Ah.........................24
     Zombiah....................28
     Hippiah....................32
     Aquah......................34
Ink drawing in October..........35
The blog........................46
Pepper&Carrot...................53
Preparing the future............68
Ending notes....................70


## Page 5

The beginning: one year before creating Pepper&Carrot (2013), the first drawing of a witch and her cat appears in my artwork.


## Page 6

Cartoonish characters were rare in my sketchbook before Pepper&Carrot, but looking back I found this one.
She is a little blacksmith's daughter with a cat. Pepper inherited a lot from this previous character's development.

A random sketch of a tiny witch appears in my sketchbook one month before posting the first episode of Pepper&Carrot. She is already looking like Pepper.

May 2014, just after posting the first episode where I designed the character on the fly, I started to draw Pepper and Carrot in my sketchbook.

## Page 7

One of the first colored illustrations of Pepper made before posting episode 2: “Rainbow Potions”. The project was still named “Mini Fantasy Theatre” at that time and published on my blog. Why this name? Probably to post stories about other characters I had drawn, and not centered around the little witch and her cat.


## Page 8

Pencil studies for settings: Pepper's house (top) and Komona's marketplace (bottom). The two drawings would be reused as-is for the first panels of episodes 2 and 3.


## Page 9

After episode 2, I found names for the characters and announced the start of a series of episodes with them. This logo provided an opportunity to reveal the names of the characters.

A slightly more dynamic pose with the angular hat and goofiness for the design of Pepper in episode 3: “The Secret Ingredient”.

The speedpainting I made on Sunday, May 4th 2014 which became the first panel for episode 1 of Pepper&Carrot.


## Page 10

At the beginning of 2015, the project moved from my blog to a dedicated website (https://www.peppercarrot.com). This was also when I designed the foundations of the translation system intended to become the home for all the of the translations that were published in the wild. I then added a philosophy page to explain my project. This header illustration for the homepage of the new website perfectly captured the energy I felt at this moment.


## Page 11

The first doodles to find two new friends of Pepper for episode 6, Shichimi and Coriander.

First storyboards for episode 6: “The Potion Contest”.

More research and the curly hat and haircut of Pepper was born. It was ready for episode 5: “Special Holiday”.


## Page 12

Various character designs for Shichimi, Saffron and Coriander between episode 6: “The Potion Challenge”, and episode 9: “The Remedy”.

Shichimi was initially supposed to have albinism. That's why she has white hair and red eyes on the early concept-art above.

I changed this design after reading more about this congenital disorder and the constraints required to protect her from sunburn and how that would affect the story. She still kept her pink eyes, but her hair was changed to bright blonde.


## Page 13

A rejected drawing for the first panel of a springtime episode.

“Read The Friendly Manual” illustration to update the header of the website.


## Page 14

Study for episode 9: “The Remedy”, featuring Pepper's home security system.

Design for a bathing suit for episode 10: “Summer Special”.

Sketches for episode 7: “The Wish”.

## Page 15

The Magic Schools

After episode six released on the new website, I published a Wiki with information about Hereva, the world of Pepper&Carrot. I started to detail my ideas about the magic system and illustrated it with the “Impossible Triangle” (above) showing the six schools of magic.


## Page 16

Pepper, Thyme, Cayenne, Cumin and Chicory.
The witches of Chaosah

A painting of Chicory, the founder of Chaosah, accidentally summoning her first black hole.

## Page 17

Episode 11: The Witches of Chaosah is when the series took an important turn. The godmother witches of Pepper: Thyme (on top middle), Cayenne (on top left) and Cumin (on top right) entered the story.

The next episode explored the relationship Pepper had with each of the witches. Episode 12 shows the confrontational relationship between Cayenne and Pepper via the black hole that Pepper created. Episode 14 shows how Cumin teaches Pepper about potion ingredients and episode 15 reveals some sneaky surprises about the night-time activities of Chaosah's leader, Thyme.

In episode 17 (right, sketched drawing for the final hug) Pepper realizes she has learned a lot from these witches. The reader will have to wait until episode 25 to witness the true power of these three witches when they rebuild their demolished house using their combined butterfly effect.


## Page 19

Sketches of Chicory and young Thyme. These might show up in a future episode as a flashback sequence about the early days of Chaosah.

The first rules of Chaosah: “A true witch of Chaosah doesn't climb the potion shelves”. This illustration shows the shelves collapsing after “the unnamed apprentice” of Chicory foolishly climbed them trying to retrieve a potion from the top shelf. She vanished into the unknown due to the multiple effects of the potions.

Doodles for the creation of the Godmothers of Chaosah.

I wanted the three old witches to be original to the series: Cayenne, the tall strict one, is inspired by Clint Eastwood while Cumin is the ideal representation of the happy and generous grandmother (as seen in France for the marketing of coffee brands, yogurts and jams). Thyme, the eldest leader, is a very short and old mysterious witch with a lot of libido.

I tried a lot of ideas and did a lot of research to find my own witches. It was very hard, because I'm a big fan of Terry Pratchett and his excellent trio of old witches from the Discworld series are deeply imprinted in my brain.


## Page 20

Saffron
The witch of Magmah

A colored illustration of Saffron for the blog, showing the evolution of her haircut and clothes.

## Page 21

Saffron was the second witch to appear in the series after Pepper. She debuted in episode 3 during the market scene in Komona. Saffron is very talented, rich, and popular. She quickly became a celebrity within the world of Pepper&Carrot.

A preparatory sketch of Saffron before the Magic Contest.

Blue drawing and line-art rendering from a panel in “The Voting System” (episode 22).


## Page 22

Drawings for Saffron for episode 16: “The Sage of the Mountain”.

Saffron selling contraband potions to a local warrior visiting Komona.


## Page 23

Panel of episode 16 showing Saffron's business, elevating witchcraft consulting as a popular commercial service.

Saffron lives in the floating city of Komona. Preparatory pencil drawing for episode 22.

A compilation of studies of poses and acting for Saffron.


## Page 24

Shichimi
The witch of Ah

Shichimi's magic -Ah- allows her to communicate with ghosts and spirits. Here, she is likely receiving special advice from a gentle spirit appearing in the vapors of her tea cup.


## Page 25

Shichimi in front of a small temple of Ah made from the bones of dragons.

A lighting color study.

Portrait of Shichimi.


## Page 26

First depiction of the land of Ah made during a painting livestream in 2016.

Shichimi is a very shy character.

Sketch and color palette research for the panel in episode 6 where Shichimi first appears.


## Page 27

No, Shichimi doesn't have horns; she rolls her long hair around a large hair piece made from the bone of a dragon. This is a tradition for a witches of Ah.

Compilation of sketches with Shichimi.

A panel from a comic page that was never published to the site, used as a demo for the hour and a half tutorial: “A Comic page from A to Z”. It features Shichimi talking to a dragon while holding a ring. She eventually throws the ring into the stream.


## Page 28

Coriander
The witch of Zombiah

First panel of episode 27: “Coriander's Invention” showing Qualicity, the city where Coriander lives.

Character design of Coriander in her workshop with Mango in her arms versus Queen Coriander with an earlier design for the crown of Qualicity.

Details of a painting in Coriander's bedroom in episode 13: “The Pyjama Party” showing a young Coriander in her royal costume, with Mango as a chick.


## Page 29

Pencil drawing for Coriander's spell during the Magic Contest.

Character design for Coriander's coronation with a crown that looks a bit more “Zombiah”.

Expression sheet and researches for Coriander.


## Page 30

Coriander in her workshop, repairing a big robot she made. I had this idea of a geeky princess since her first appearance in the potion contest. This drawing from 2017 was a way to show this side of her until I reached the first episode of the coronation, published one year later.


## Page 31

Coriander's workshop, a concept-art that became a panel in episode 27: “Coriander's Invention”.

Design for the trio of friends before starting the production of the coronation arc. The characters have slightly aged before this episode. It was a challenge to learn how to draw them older.


## Page 32

Camomile, Cinnamon and Quassia
The witches of Hippiah

Flashback panel scene from episode 18: “The Encounter” with Quassia, the teacher, reviewing the spells of Cinnamon (left) and Camomile (right). Camomile is the only half-human, half-raccoon witch. Cinnamon is an elf with red hair.

Studies for a Hippiah building that was never used in the webcomic.

Portrait of Cinnamon as a grown-up. I wanted her to have a part in the coronation arc, but I had to cut her sequence for time-budget reasons.


## Page 33

Study of Pepper during her first years as a witch of Hippiah.

A portrait of Camomile playing with a swarm of fairies. She really has a special connection with nature.

Concept-art pencil drawing for the school of Hippiah: a farm-school in perfect symbiosis with the surrounding environment.


## Page 34

Spirulina
The witch of Aquah

Character design of Spirulina before the Magic Contest arc. Her familiar is a male Betta fish named Durian.

Drawing (overlay) of the spell of Spirulina during the Magic Contest.

Drawing in black and white ink of Spirulina and Durian, a perfect transition to our next chapter...


## Page 35

Ink Drawing
During the Month of
October


## Page 37

In October 2015 and 2017 I joined an online challenge to post a drawing made with ink on social media every day for a month. (I'm not mentioning the exact name of the challenge in this book because the creator of this challenge has trademarked the name).

It quickly became one of the largest sources of Pepper&Carrot drawings created, aside from the artworks produced for the panels of the episodes. They comprise one of the largest sources of art used to compile this artbook.

Pepper and Carrot on the edges of the Komona city, looking at the clouds.

Pepper's way of using her broom (much to Carrot's chagrin).

Carrot and the donkey from episode 20: “The Picnic” battle in the style of Don Quixote against a giant vacuum cleaner monster.

Rivalry between between Pepper and Saffron illustrated.


## Page 39

Bath time for Carrot. Pepper can try, but Carrot will not go without a fight.

Pepper taking a very angry Carrot to the vet.


Pepper and Carrot as kids, stealing the nuts from a living tree.

Pepper knitting the world's longest sock (did you notice the outstretched paw of Carrot in the foreground?)

Pepper selling potions at the market.


## Page 40

Landing training for Pepper!

Do you remember the Giant Posh Zombie Canary bird from episode 6: “The Potion Contest”? I imagined her trying to return to normal life on an island. It doesn't look that easy.

Below(right): Carrot is impressed by the system this little mole found to walk in the garden.


Carrot invited Truffel (Saffron's cat) on a date to a fancy restaurant but she was expecting to dine inside the restaurant.

Carrot and a mouse wrecked Pepper's laboratory. They both have good reasons to blame the other one for starting the fight.


## Page 42

Pepper posing into the wind.


## Page 43

Pepper found her old doll in the attic; a real treasure for her.

Pepper and Carrot jumping into a pile of leaves.

A depiction of the forest around Pepper's home. Of course, Carrot is sleeping when Pepper manages to find him.


## Page 44

Shichimi, Pepper, and Coriander: United.

It's summertime and Pepper used a potion of growth to make a giant orange so she could get more orange juice! Unfortunately, Carrot doesn't understand the appeal.

Concept idea for Coriander on her flying ship. This uses the same floating technology as the Great Tree at the center of Komona.


## Page 46

The Blog

In-between the production of new episodes, I write blog posts to inform the audience about my progress. These blog posts are often illustrated with artworks.

The illustration for the release of the Code of Conduct for the Pepper&Carrot project.

Part of a blog-post about refactoring, making difficult choices, and big maintenance tasks.

I also create drawings for special dates: holidays, New Years Day, and the obligatory special day for a series about witches: Halloween.


## Page 47

A mini-comic for Halloween, 2016.

Artwork for the New Year, 2015.


## Page 48

Double page: Compilation of New Year artworks.


## Page 50

To celebrate the holiday time in December, I made a parody of Xmas in the world of Pepper&Carrot. They celebrate the Unity of Hereva and decorate their house with a Unity tree. You'll find more about it in episode 24: “The Unity Tree”.

Pepper makes a Unity tree with magic, 2016.

Pepper and Carrot decorating a Unity tree.


## Page 51

The blog is also a place for me to express my happiness and share good news, and because a picture is worth 1000 words...

A Thank You posted for the contributors who sent donations right after the burglary of my house in 2017. My hardware was stolen and I couldn't finish the Magic Contest on time. The end of the month was difficult but this spontaneous wave of donations really helped me to get through it on all levels.

A party illustration to celebrate having more than 500 patrons on the project.


## Page 53

Pepper&Carrot

The relationship between my main characters is often my topic of choice when I do a sketch or an artwork aside from the webcomic production. That's why I've collected a lot of material about them. You'll see that on the following pages.


## Page 54

A color rendering of a sketch of Pepper I had in my sketchbook. This sketch, made before episode 23: “Take a Chance”, marked a turn into the way I designed Pepper. Under her mask of goofiness she hides a fragile and not-so confident personality.


## Page 55

Expression sheet for Carrot: research for extreme reactions and emotions.

Model-sheet for Pepper around episode 26: “Books Are Great”.


## Page 56

Artwork used when the website is in maintenance mode.

Sketch of Pepper thinking with a cup of hot chocolate after a hard day. She looks surprised; maybe she saw something in her cup?

Speed-painting featuring Carrot done during a one hour livestream workshop about painting dragons for the Libre Graphics Meeting Online, 2020.


## Page 57

“Rainy days”, a piece I made around episode 22 when I wasn't in the mood.


## Page 58

“Dawn”, an artwork made to explore new body proportions for the young adult Pepper.


## Page 59

Carrot wants to say he loves you! I published that to say a big “thank you” to the supporters and contributors of the project.

Portrait of Pepper before the battlefield arc (episodes 32 and 33). This artwork was research for a more chaotic hat (similar to Thyme's hat). I tried to show Pepper wearing the Chaosah infinity sign around her neck like Cayenne does, and a collar made of bones like Thyme has.

Study for a ninja-Carrot, an idea for an episode that was never finished.


## Page 60

Drawing that accompanied a blog post for the release of a podcast interview on librelounge.org.

Thumbnail color studies to find new color palettes and compositions.

A double-page from my sketchbook to study Pepper's expressions in episode 24: “The Unity Tree”.


## Page 61

A speed-painting of Pepper before episode 31: “The Fight”.


## Page 62

Before the production of episode 31, with Pepper's final exam and the epic battle with Cayenne, I wanted to see how a powerful Pepper would work in full focus for a fight. Once I had this illustration, the episode was easier to produce.


## Page 63

Digital pencil concept artwork of Pepper I drew after the coronation arc. I was researching a silent episode for episode 30, with Pepper finding a magic staff and beginning a quest to return it to its owner. I'll keep the idea for a future episode, maybe.


## Page 64

One of the challenges of a series with a witch for the main character is finding good poses for her to ride a broom stick.

Sketches to search for better poses and viewing angles while keeping her big smile and overall fresh energy.

Thumbnail studies for the cover of this book.

One illustration of Pepper flying above her house made to refresh the header image of the website.


## Page 66

Above: Character designs for Pepper in the Battlefield arc.


## Page 67

Above and below: Initial drawings, concept-art and designs for episode 32: “The Battlefield”.


## Page 68

Preparing for the future

When I collected pictures of this book in my sketchbook, I realized that many of them were not directly related to Pepper&Carrot. That's because the main purpose of producing such sketches is to give myself a lot of options for future scenarios. So, here is just a tiny sample of these sketches. I'll save the rest for a (possible) future book.

Studies for random new characters: (left) a witch of Hippiah named Marjoram, and (right) a witch of Magmah using her magic as an entertainer.

Giant mushroom architecture.

A young bounty hunter traveling the world on an old dragon with a passion for reading.


## Page 69

~Fin~


## Page 70

Ending notes

I hope you liked this collection of artwork as much as I enjoyed collecting, commenting, and arranging them into this book. I wanted to make this book more than just a silent gallery of artworks. I wanted this to be an invitation for you to discover new aspects about the world of Pepper&Carrot that would complement the webcomic episodes.

Working on this large retrospective of both published and unpublished artwork from Pepper&Carrot was also a perfect opportunity for me pause and reflect about the series. All in all, I spent a lot of my summer in 2020 scanning sketchbooks, cleaning the scans, improving the quality of the artwork for print, color-correcting the artwork, and working on the layout. Of course, you'll find all of the sources for the artwork of this book on the official website under the “Sources” category in the main menu. As always, they're released under the same permissive Creative Commons Attribution 4.0 International license as the rest of the comics.

After finishing a book like this, I have so many new stories in mind for Pepper&Carrot. Thank you very much for being around and for your accompaniment and support.




## Page 71

Desktop publishing done with Scribus 1.5.5 on Kubuntu Linux 20.04
Montauban, France - July, 2020
www.davidrevoy.com

www.peppercarrot.com
