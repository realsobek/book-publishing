#!/bin/bash
#: Title        : Bookpublishing file downloader, using Pepper&Carrot API
#: Author       : David REVOY < info@davidrevoy.com >
#: License      : GPLv3 or higher
#: Dependencies : curl wget jq imagemagick inkscape 1.0 grep md5sum
# -----------------------------------------------------
#: Require Pepper&Carrot website and internet connexion
#: Download 2GB
#: /!\ Require 4GB minimum of disk /!\
#: /!\ Can take hours to render the 200 pages /!\
# -------------------------------------------------
# USAGE:
# 1. Run the script at the root of the repository; it take care of everything.
# The script has a smart cache that will download and re-render only the
# translations pushed on peppercarrot/webcomics repo.

# colors
Off=$'\e[0m'
Red=$'\e[1;31m'
Green=$'\e[1;32m'
Blue=$'\e[1;34m'
Yellow=$'\e[1;33m'

# terminal windows title
printf "\033]0;%s\007\n" "Bookpublishing - Rendering"


# Setup the root directory
input_path="${PWD}"

# Setup the target language
lang="${LANG:=en}"

# Debug mode: 1=on=tests , 0=off=production (without cache, for testing)
debugmode="${DEBUG:=0}"

# Method to create new folders if not existing
# ============================================
_create_folder () {
  FOLDERPATH=$1
  FULLPATH="$input_path/$FOLDERPATH"
  # Create the directory if missing
  if [ ! -d "$FULLPATH" ]; then
      echo "${Green}=>Missing target directory${Off}"
      echo "Creating" "$FULLPATH"
      mkdir -p "$FULLPATH"
      # Check if it was created
      if [ ! -d "$FULLPATH" ]; then
        echo "${Red}=>Error: coudn't create directory. Please check permissions. ${Off}"
        exit
      fi
  else
    echo "* $FOLDERPATH found."
  fi
}

# Method to delete a file only if it is exist
# ===========================================
_delete_secure () {
  PATHTODELETE=$1
  # exit if empty
  if [ -z ${PATHTODELETE} ]; then
    echo "${Red} security exit: a variable for deleting is undefined or dangerous.${Off}"
    exit 1
  fi
  # file
  if [ -f ${PATHTODELETE} ]; then
    # echo "* Deleting file ${PATHTODELETE}"
    rm ${PATHTODELETE}
  fi
  # folder
  if [ -d ${PATHTODELETE} ]; then
    # echo "* Deleting folder ${PATHTODELETE}"
    rm -r ${PATHTODELETE}
  fi
}

# Method to convert an input RGB image (jpg or png), into a ready to print CMYK+hald-clut+sharpening Tiff.
# eg. _convert_to_tiff ${tmp_folder}/${basename}.png ${input_path}/tiff/${lang}/${basename}.tiff
# ========================================================================================================
_convert_to_tiff () {
  ImgInputWithPath=$1
  outputpath=$2
  rgbinputfile=$(basename $ImgInputWithPath)
  RgbSanifiedAlpha=$(echo $rgbinputfile|sed 's/\(.*\)\..\+/\1/')"_alphaoff"
  rgbinputfilenoext=$(echo $rgbinputfile|sed 's/\(.*\)\..\+/\1/')""

  # 0. Remove alpha of the picture (alpha in CMYK is no sens for PDF-X)
  convert ${ImgInputWithPath} -background white -alpha remove ${tmp_folder}/${RgbSanifiedAlpha}.mpc

  # a. Apply the hald-clut
  convert -strip -interlace Plane -profile ${input_path}/src/icc/sRGB-elle-V2-srgbtrc.icc ${tmp_folder}/${RgbSanifiedAlpha}.mpc ${input_path}/src/icc/hald-clut16.png -hald-clut ${tmp_folder}/${rgbinputfilenoext}.mpc

  # b. Sharpen the rendering while adapted, lossless and still sRGB
  convert ${tmp_folder}/${rgbinputfilenoext}.mpc -unsharp 0x4+0.50+0 ${tmp_folder}/${rgbinputfilenoext}.mpc

  # c. Convert to CMYK
  convert ${tmp_folder}/${rgbinputfilenoext}.mpc -profile ${input_path}/src/icc/sRGB-elle-V2-srgbtrc.icc -intent Relative -black-point-compensation -profile ${input_path}/src/icc/CGATS21_CRPC1.icc -density 300 -units pixelsperinch -compress zip ${outputpath}

  # d. Clean-up temporary files created by imagemagick
  _delete_secure ${tmp_folder}/${RgbSanifiedAlpha}.mpc
  _delete_secure ${tmp_folder}/${rgbinputfilenoext}.mpc
  _delete_secure ${tmp_folder}/${rgbinputfilenoext}.cache
}

# Method to render a RGB image using cache (to avoid doing it twice if source hasn't changed).
# It require the directory name (OR "src" OR "artworks" OR "publishing" OR ${lang} ) to know where to store the cache and where to render.
# Eg. _render_with_cache /path/to/my/file.jpg "artworks"
# =========================================================================================================================================
_render_with_cache () {
  imginput=$1
  imgfile=$(basename $imginput)
  renderdirectorytarget=$2
  txtfile=$(echo $imgfile|sed 's/\(.*\)\..\+/\1/')".txt"
  tiffoutput=$(echo $imgfile|sed 's/\(.*\)\..\+/\1/')".tiff"

  # Caching system: store a checksum of the file in memory
  md5read="`md5sum ${input_path}/src/${renderdirectorytarget}/${imgfile}`"

  # check if cache file exist on disk (if not: create a blank)
  if [ -f ${input_path}/cache/${renderdirectorytarget}/${txtfile} ]; then
    true
  else
    touch ${input_path}/cache/${renderdirectorytarget}/${txtfile}
  fi

  # Compare if checksum in memory is similar to checksum on disk read from cache file
  if grep -q ${md5read} ${input_path}/cache/${renderdirectorytarget}/${txtfile}; then
    true
  else
    echo "${Green}* Rendering ${renderdirectorytarget}/${imginput} to CMYK tiff${Off}"
    echo "----------------------------------------------------------------------------------"

    # The cache was different, we render to tiff
    _convert_to_tiff ${input_path}/src/${renderdirectorytarget}/${imginput} ${input_path}/tiff/${renderdirectorytarget}/${tiffoutput}

    # When done, update the cache
    md5sum ${input_path}/src/${renderdirectorytarget}/${imgfile} > ${input_path}/cache/${renderdirectorytarget}/${txtfile}
  fi
}

# Debug mode: flush the cash at each cycle to simplify testing
# ============================================================
if [ ${debugmode} == 1 ]; then
  echo "${Red}== Debug mode ON ===${Off}"
  _delete_secure ${input_path}/tiff/${lang}
  _delete_secure ${input_path}/cache/${lang}
  _delete_secure ${input_path}/src/${lang}
  cd ${input_path}/src/
  for IMGTOCLEAN in *.png ; do
    _delete_secure ${IMGTOCLEAN}
  done
  for IMGTOCLEAN in *.jpg ; do
    _delete_secure ${IMGTOCLEAN}
  done
fi


# Tmp area
tmp_folder="${TEMP_PATH:=/tmp}/peppercarrot_book-publishing-tmp-jobs"
# cleanup in case a last run was aborted mid-way.
_delete_secure ${tmp_folder}
# start new
mkdir -p ${tmp_folder}
if [ ! -d ${tmp_folder} ]; then
  echo "${Red} Check permission to create ${tmp_folder} . Exit: the script can't run without a temporary directory.${Off}"
  exit 1
fi

# Source tree:
_create_folder "src/icc"
_create_folder "src/${lang}"
_create_folder "src/artworks"
_create_folder "src/publishing"
_create_folder "src/vector"
# Tiff tree:
_create_folder "tiff/${lang}"
_create_folder "tiff/artworks"
_create_folder "tiff/publishing"
# Cache tree
_create_folder "cache/${lang}"
_create_folder "cache/artworks"
_create_folder "cache/publishing"

# Enter the src directory
cd ${input_path}/src

# Download the JSON API
echo "${Blue}""* [JSON]  https://www.peppercarrot.com/0_sources/episodes-v1.json""${Off}"
wget -N 'https://www.peppercarrot.com/0_sources/episodes-v1.json'

# Set a counter for looping threw the JSON
count=0

# Download the Hald-Clut for conversion and profiles
cd ${input_path}/src/icc/
wget -N -q --show-progress https://www.peppercarrot.com/0_sources/0ther/book-publishing/icc/hald-clut16.png
wget -N -q --show-progress https://www.peppercarrot.com/0_sources/0ther/book-publishing/icc/CGATS21_CRPC1.icc
wget -N -q --show-progress https://www.peppercarrot.com/0_sources/0ther/book-publishing/icc/sRGB-elle-V2-srgbtrc.icc

_renderall_comicpages () {
  # Enter the target directory
  cd ${input_path}/src/${lang}/

  # Loop threw the JSON for episode folders
  cat '../episodes-v1.json' | jq -r '.[].name' | while read episodeFolder ; do
    # Limit the loop to episode 29
    if [ $count -lt 29 ]; then
      # Print the episode folder's name
      echo ""
      echo "${Green} #################################### ${Off}"
      echo "${Green}     [ $episodeFolder ] ${Off}"
      echo "${Green} #################################### ${Off}"
      echo ""

      # Loop threw each episodes and list all available pages filename
      cat '../episodes-v1.json' | jq -r '.['$count'].pages | .[]' | while read pageUrl ; do
        # Cleanup filename
        targetFilename=${pageUrl#"Pepper-and-Carrot_by-David-Revoy_"}
        # Setup new filenames based on input
        basename=$(echo $targetFilename|sed 's/\(.*\)\..\+/\1/')""
        pageUrlModified=$(echo $pageUrl|sed 's/\(.*\)\..\+/\1/')"-output.jpg"
        pageUrlPNG=$(echo $pageUrl|sed 's/\(.*\)\..\+/\1/')".png"
        pageUrlJPG=$(echo $pageUrl|sed 's/\(.*\)\..\+/\1/')".jpg"
        txtfile=$(echo $pageUrl|sed 's/\(.*\)\..\+/\1/')".txt"
        svgSource=${basename}".svg"
          # Filter a rule if our URL contains .gif extension
          if [[ $pageUrl == *".gif"* ]]; then
            # We have a gif
            # Download a special PNG (we don't want gif animation for the book)
            cd ${input_path}"/src"
            echo "${Green}* [GIF]${Off}"
            # Get a special PNG substitute for the gif at root of the episode
            wget -N -q --show-progress https://www.peppercarrot.com/0_sources/${episodeFolder}/${basename}.png
            if [ -f ${input_path}/src/${basename}.png ]; then
              # Assure the image to be at least fitting in a 2481x2481box (300ppi size):
              convert ${input_path}/src/${basename}.png -resize 2481x2481 ${tmp_folder}/${basename}.png

              # Rendering to tiff
              _convert_to_tiff ${tmp_folder}/${basename}.png ${input_path}/tiff/${lang}/${basename}.tiff

              # Clean-up temporary png
              _delete_secure ${tmp_folder}/${basename}.png
            fi
          else

            # Find the name of the endpage and cover
            endpage=$(cat $input_path'/src/episodes-v1.json' | jq -r '.['$count'].pages | .credits')
            coverpage=$(cat $input_path'/src/episodes-v1.json' | jq -r '.['$count'].pages | .cover')

            # Don't process "credits" (last page) of each episode: it's not necessary
            if [[ $pageUrl != $endpage ]]; then
              # Don't process page 00 (title): not necessary
              if [[ ${basename} != E??P00 ]]; then

                # Download ART artworks
                cd ${input_path}/src/

                # Print a title
                echo "${Green}================== ${basename} ==================${Off}"

                # Exception: for top title/cover
                if [ $pageUrl == $coverpage ]; then
                  # Generate a b&w title from txt-only:
                  echo "${Blue}* [TOP-TITLE/COVER] Download: https://www.peppercarrot.com/0_sources/${episodeFolder}/hi-res/txt-only/${lang}_${pageUrlPNG} ${Off}"
                  # 1. The top title:
                  wget -N -q --show-progress https://www.peppercarrot.com/0_sources/${episodeFolder}/hi-res/txt-only/${lang}_${pageUrlPNG}
                  # If found:
                  if [ -f ${input_path}/src/${lang}_${pageUrlPNG} ]; then
                    # Crop, invert, remove alpha
                    convert ${input_path}/src/${lang}_${pageUrlPNG} -negate -gravity South -crop 1100x65+0+15 +repage -background white -alpha remove ${input_path}/src/gfx_Pepper-and-Carrot_by-David-Revoy_${basename}b.png
                    # Convert to CMYK
                    convert ${input_path}/src/gfx_Pepper-and-Carrot_by-David-Revoy_${basename}b.png -profile ${input_path}/src/icc/sRGB-elle-V2-srgbtrc.icc -intent Relative -black-point-compensation -profile ${input_path}/src/icc/CGATS21_CRPC1.icc -density 300 -units pixelsperinch -compress zip ${input_path}/tiff/${lang}/${basename}P00.tiff
                  fi
                  # 2. The cover
                  wget -N https://www.peppercarrot.com/0_sources/${episodeFolder}/hi-res/${lang}_${pageUrlJPG}
                  if [ -f ${input_path}/src/${lang}_${pageUrlJPG} ]; then

                    # Rendering to tiff
                    _convert_to_tiff ${input_path}/src/${lang}_${pageUrlJPG} ${input_path}/tiff/${lang}/${basename}-cover.tiff

                  fi

                fi

                # It's a regular comic page
                echo "${Blue}* [ART] Download: https://www.peppercarrot.com/0_sources/${episodeFolder}/hi-res/gfx-only/lossless/gfx_${pageUrlPNG} ${Off}"
                wget -N -q --show-progress https://www.peppercarrot.com/0_sources/${episodeFolder}/hi-res/gfx-only/lossless/gfx_${pageUrlPNG}


                # Generate a checksum
                md5read="`md5sum ${input_path}/src/gfx_${pageUrlPNG}`"
                # Avoid grep to fail if no file found
                if [ -f ${input_path}/cache/${lang}/gfx_${txtfile} ]; then
                  true
                else
                  touch ${input_path}/cache/${lang}/gfx_${txtfile}
                fi

                # Compare if actual checksum is similar to the previous one recorded on txtfile
                if grep -q ${md5read} ${input_path}/cache/${lang}/gfx_${txtfile}; then
                  true
                else
                  echo "${Yellow} ==> new or modified: empty SVG cache for re-render. ${Off}"
                  # empty SVG cache for re-render
                  if [ -f ${input_path}/cache/${lang}/${txtfile} ]; then
                    rm ${input_path}/cache/${lang}/${txtfile}
                  fi
                  # Update the cache with a new version
                  md5sum ${input_path}/src/gfx_${pageUrlPNG} > ${input_path}/cache/${lang}/gfx_${txtfile}
                  echo ""
                fi

                # Download SVG translation
                # Exception: not if a cover
                if [ $pageUrl == $coverpage ]; then
                  true
                else

                  cd ${input_path}/src/${lang}
                  echo "${Blue}* [SVG] Download: https://www.peppercarrot.com/0_sources/${episodeFolder}/lang/${lang}/${svgSource} ${Off}"
                  wget -N -q --show-progress https://www.peppercarrot.com/0_sources/${episodeFolder}/lang/${lang}/${svgSource}
                  # Generate a checksum
                  md5read="`md5sum ${input_path}/src/${lang}/${svgSource}`"

                  # Avoid grep to fail if no file found
                  if [ -f ${input_path}/cache/${lang}/${txtfile} ]; then
                    true
                  else
                    touch ${input_path}/cache/${lang}/${txtfile}
                  fi

                  # Compare if actual checksum is similar to the previous one recorded on txtfile
                  if grep -q ${md5read} ${input_path}/cache/${lang}/${txtfile}; then
                    true
                  else

                    echo "${Yellow} ==> new or modified (rendering). ${Off}"

                    # Render Inkscape (PNG)
                    inkscape ${input_path}/src/${lang}/${svgSource} -o ${tmp_folder}/${basename}.png

                    # Convert comic page to CMYK
                    # --------------------------
                    # 1. Apply _mask to the page if the page has a mask
                    if [ -f ${input_path}/src/_mask/${basename}.png ]; then
                      echo "${Yellow} ==> Masking part of translation with src/_mask. ${Off}"
                      # a. Apply lossless compositing
                      convert ${input_path}/src/gfx_"$pageUrlPNG" ${tmp_folder}/${basename}.png ${input_path}/src/_mask/${basename}.png -composite ${tmp_folder}/${basename}.mpc

                      # b. Ensure building a better PNG after compositing
                      convert -strip -interlace Plane -units PixelsPerInch ${tmp_folder}/${basename}.mpc -density 300 -colorspace sRGB -background white -alpha remove -define png:compression-level=1 ${tmp_folder}/${basename}.png
                      convert ${tmp_folder}/${basename}.png -background white -alpha remove -alpha off ${tmp_folder}/${basename}.png
                    fi

                    # Rendering to tiff
                    _convert_to_tiff ${tmp_folder}/${basename}.png ${input_path}/tiff/${lang}/${basename}.tiff

                    # Clean-up temporary png file
                    _delete_secure ${tmp_folder}/${basename}.png

                    # Update the cache with a new version
                    md5sum ${input_path}/src/${lang}/${svgSource} > ${input_path}/cache/${lang}/${txtfile}

                    echo ""
                  fi
                fi
              fi
            fi
          fi
      done
    fi

  # Increment the counter for getting into the next episode at the next loop
  count=$((count + 1))

  done
}



_renderall_artworks () {
  # Download other sources from https://www.peppercarrot.com/en/static6/sources
  echo "${Green} #################################### ${Off}"
  echo "${Green}            [ OTHER SOURCES ]         ${Off}"
  echo "${Green} #################################### ${Off}"
  echo ""

  # Vector art, svg with spot colors (no need to render)
  cd ${input_path}/src/vector/
  wget -r --accept svg,SVG --level=1 -nd -N -q --show-progress https://www.peppercarrot.com/0_sources/0ther/vector/
  # Book covers and graphics specific for the book:
  cd ${input_path}/src/publishing/
  wget -r --accept png,PNG --level=1 -nd -N -q --show-progress https://www.peppercarrot.com/0_sources/0ther/book-publishing/hi-res/
  wget -N -q --show-progress https://www.peppercarrot.com/0_sources/0ther/vector/hi-res/decorative-arrows_by-David-Revoy.png
  # Artworks:
  cd ${input_path}/src/artworks
  wget -r --accept jpg,JPG --level=1 -nd -N -q --show-progress https://www.peppercarrot.com/0_sources/0ther/artworks/hi-res/
  wget -r --accept jpg,JPG --level=1 -nd -N -q --show-progress https://www.peppercarrot.com/0_sources/0ther/sketchbook/hi-res/
  wget -N -q --show-progress https://www.peppercarrot.com/0_sources/0ther/misc/hi-res/2016-04-13_carrot-updating-or-repairing_by-David-Revoy.jpg
  wget -N -q --show-progress https://www.peppercarrot.com/0_sources/0ther/misc/hi-res/2016-03-08_fairy-on-rpg-dice-20_by-David-Revoy.jpg
  wget -N -q --show-progress https://www.peppercarrot.com/0_sources/0ther/misc/hi-res/2016-06-06_environment-shichimi_ep17_livestream_by-David-Revoy.jpg
  wget -N -q --show-progress https://www.peppercarrot.com/0_sources/0ther/misc/hi-res/2016-06-08_concept-art_e17_Phanda_livestreaming_by-David-Revoy.jpg
  wget -N -q --show-progress https://www.peppercarrot.com/0_sources/0ther/misc/hi-res/2015-02-09_philosophy_01-support_by-David-Revoy.jpg
  wget -N -q --show-progress https://www.peppercarrot.com/0_sources/0ther/misc/hi-res/2015-02-09_philosophy_03-paywall_by-David-Revoy.jpg
  wget -N -q --show-progress https://www.peppercarrot.com/0_sources/0ther/misc/hi-res/2015-02-09_philosophy_04-open-source_by-David-Revoy.jpg
  wget -N -q --show-progress https://www.peppercarrot.com/0_sources/0ther/misc/hi-res/2015-02-09_philosophy_05-everyone_by-David-Revoy.jpg
  wget -N -q --show-progress https://www.peppercarrot.com/0_sources/0ther/misc/hi-res/2015-02-09_philosophy_06-industry-change_by-David-Revoy.jpg
  wget -N -q --show-progress https://www.peppercarrot.com/0_sources/0ther/misc/hi-res/2015-03-17_happy-dance_by-David-Revoy.jpg
  wget -N -q --show-progress https://www.peppercarrot.com/0_sources/0ther/misc/hi-res/2016-11-29_wiki_place_hereva_by-David-Revoy.jpg
  wget -N -q --show-progress https://www.peppercarrot.com/0_sources/0ther/misc/hi-res/2018-09-20_the-coronation_by-David-Revoy.jpg
  wget -N -q --show-progress https://www.peppercarrot.com/0_sources/0ther/misc/hi-res/2019-05-09_Podcast-illustration_by-David-Revoy.jpg
  wget -N -q --show-progress https://www.peppercarrot.com/0_sources/0ther/misc/hi-res/2020-01-10_Hug-sticker-for-Signal_by-David-Revoy.jpg
  wget -N -q --show-progress https://www.peppercarrot.com/0_sources/0ther/misc/hi-res/2020-05-29_Jackfruit-and-Carrot_LGMlivestream_by-David-Revoy.jpg
  wget -N -q --show-progress https://www.peppercarrot.com/0_sources/0ther/press/hi-res/2014-06-18_logo_by-David-Revoy.jpg
  wget -N -q --show-progress https://www.peppercarrot.com/0_sources/0ther/press/hi-res/2015-10-12_logo_by-David-Revoy.jpg
  wget -N -q --show-progress https://www.peppercarrot.com/0_sources/0ther/press/hi-res/2015_Portrait-of-David-Revoy_by-Elisa_de_Castro_Guerra_by-David-Revoy.jpg
  wget -N -q --show-progress https://www.peppercarrot.com/0_sources/0ther/wallpapers/2560x1600/Episode-1_peppercarrot-wallpaper_by-David-Revoy.jpg
  wget -N -q --show-progress https://www.peppercarrot.com/0_sources/0ther/wallpapers/2560x1600/Episode-7_peppercarrot-wallpaper_by-David-Revoy.jpg
  wget -N -q --show-progress https://www.peppercarrot.com/0_sources/0ther/wallpapers/2560x1600/Episode-25-page-1_peppercarrot-wallpaper_by-David-Revoy.jpg
  wget -N -q --show-progress https://www.peppercarrot.com/0_sources/0ther/wallpapers/2560x1600/Episode-24_peppercarrot-wallpaper_by-David-Revoy.jpg
  wget -N -q --show-progress https://www.peppercarrot.com/0_sources/0ther/wallpapers/2560x1600/Episode-25-page-4_peppercarrot-wallpaper_by-David-Revoy.jpg
  wget -N -q --show-progress https://www.peppercarrot.com/0_sources/0ther/wallpapers/2560x1600/Episode-25-page-9_peppercarrot-wallpaper_by-David-Revoy.jpg
  wget -N -q --show-progress https://www.peppercarrot.com/0_sources/0ther/wallpapers/2560x1600/Episode-27-page-3_peppercarrot-wallpaper_by-David-Revoy.jpg
  wget -N -q --show-progress https://www.peppercarrot.com/0_sources/0ther/wallpapers/2560x1600/Episode-27-page-1_peppercarrot-wallpaper_by-David-Revoy.jpg
  wget -N -q --show-progress https://www.peppercarrot.com/0_sources/0ther/wallpapers/2560x1600/Komona-sunset_peppercarrot-wallpaper_by-David-Revoy.jpg
  wget -N -q --show-progress https://www.peppercarrot.com/0_sources/0ther/wallpapers/2560x1600/Shichimi-and-Yuzu_peppercarrot-wallpaper_by-David-Revoy.jpg
  wget -N -q --show-progress https://www.peppercarrot.com/0_sources/0ther/wallpapers/2560x1600/Snow-Angels_peppercarrot-wallpaper_by-David-Revoy.jpg
  wget -N -q --show-progress https://www.peppercarrot.com/0_sources/0ther/wallpapers/2560x1600/The-micro-dimension_peppercarrot-wallpaper_by-David-Revoy.jpg
  wget -N -q --show-progress https://www.peppercarrot.com/0_sources/0ther/eshop/hi-res/2019-09-24_carrot-eshop_by-David-Revoy.png
  wget -N -q --show-progress https://www.peppercarrot.com/0_sources/0ther/eshop/hi-res/2019-09-25_peppercarrot-eshop_by-David-Revoy.png

  # Batch convert the files downloaded.
  cd ${input_path}/src/artworks
  for imgjpginput in *.jpg ; do
    _render_with_cache ${imgjpginput} "artworks"
  done

  cd ${input_path}/src/publishing/
  for imgpnginput in *.png ; do
    _render_with_cache ${imgpnginput} "publishing"
  done

  # Exit the directory and go back to script's root
  cd ${input_path}

  # Cleaning tmp folder
  _delete_secure ${tmp_folder}
  # Cleaning robot.txt.tmp*
  find ${input_path}/src/ \( -name "robots.txt.tmp*" \) -delete
}

# You can comment the functions under if you need only to re-render the pages
# or if you need to re-render all the artworks/publishing/sketchbook
if [ "$1" = "--nocomicrender" ]; then
  echo "option --nocomicrender detected: skip them"
else
  _renderall_comicpages
fi
_renderall_artworks


# Invite to reload the script if launched as standalone. Bypass if called from external
# script with --noprompt flag.
if [ "$2" = "--noprompt" ]; then
  echo "option --noprompt detected: skip end menu"
else
  echo "${Blue}================== JOB DONE ==================${Off}"
  echo "${Purple}  Press [Enter] to launch the script again or:"
  echo "${White}${BlueBG}[q]${Off} => to Quit. ${Off}"
  read -p "?" ANSWER
  if [ "$ANSWER" = "q" ]; then
    exit
  else
    # Launch again
    ${input_path}/download.sh
  fi
fi
