#!/bin/bash
rm -rf tiff
rm -rf src/artworks
rm -rf src/icc
rm -rf src/publishing
rm -rf src/vecotr
rm -rf src/*.jpg
rm -rf src/*.png
rm -rf src/*.json
rm -rf src/*.tmp
rm -rf src/*.tmp.*
rm -rf "src/${LANG:=en}"
rm -rf cache
